<?php

namespace BaseBot;

use \Mediawiki\Api\{
    MediawikiApi,
    ApiUser,
    SimpleRequest,
    UsageException
};
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\Connection;

class Bot
{
    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var MediawikiApi
     */
    public $api;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * @var string
     */
    private $token;

    /**
     * @return mixed
     */
    private function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    private function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * Bot constructor.
     *
     * @param array $config
     * @param LoggerInterface $logger
     *
     * @throws \Mediawiki\Api\UsageException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct(array $config, LoggerInterface $logger)
    {
        $this->setLogin($config['username']);
        $this->setPassword($config['password']);
        $this->logger = $logger;


        $this->api = MediawikiApi::newFromApiEndpoint('https://uk.wikipedia.org/w/api.php');
        $this->api->setLogger($this->logger);

        $this->logger->info("Attempting to log in");
        $this->api->login(new ApiUser($this->getLogin(), $this->getPassword()));
        $this->logger->info("Successfully logged in");

        $this->connection = $this->createConnection($config['connection']);
    }

    /**
     * @param array $connectionParams
     *
     * @return Connection
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function createConnection(array $connectionParams): Connection
    {
        $config = new \Doctrine\DBAL\Configuration();

        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

        return $conn;
    }

    /**
     */
    public function run(): void
    {
        $this->logger->info("Starting the main process");

        $files = $this->getWorkingList();

        $counter = 0;

        foreach ($files as $file) {
            $counter++;

            /** @var array $logParams */
            $logParams = unserialize($file['log_params']);
            $this->unreview($logParams[0], 'Відкіт масового патрулювання файлів зробленого Shmurak');

            if ($counter >= 10) {
                break;
            }
        }

        echo count($files) . PHP_EOL;
    }

    /**
     * @return null|string
     */
    public function getCsrfToken(): ?string
    {
        $tokenRequest = new SimpleRequest(
            'query',
            [
                'meta' => 'tokens'
            ]
        );

        try {
            $response = $this->api->postRequest($tokenRequest);
        } catch (UsageException $e) {
            $this->logger->error('The api returned an error:' . PHP_EOL . $e->getMessage());
        }

        return $response['query']['tokens']['csrftoken'] ?? null;
    }

    /**
     * @param int $revid
     * @param string $comment
     */
    public function unreview(int $revid, string $comment): void
    {
        if (empty($this->token)) {
            $this->token = $this->getCsrfToken();
        }

        $unreviewRequest = new SimpleRequest(
            'review',
            [
                'revid' => $revid,
                'comment' => $comment,
                'unapprove' => 1,
                'flag_accuracy' => 1,
                'token' => $this->token,
            ]
        );


        try {
            $this->logger->debug('Making simple API request:' . PHP_EOL . print_r($unreviewRequest->getParams(), true));
            $this->api->postRequest($unreviewRequest);
        } catch (UsageException $e) {
            $this->logger->error('The api returned an error:' . PHP_EOL . $e->getMessage());
        }
    }


    /**
     * @return array
     */
    public function getWorkingList(): array
    {
        return $this->fetchReviewedFiles();
    }

    /**
     * @return array
     */
    private function fetchReviewedFiles(): array
    {
        return $this->connection->fetchAll('
            select log_title, log_namespace, log_user_text, log_action, log_params
            from logging_userindex
            join page 
            on log_user_text = \'Shmurak\'
            and log_type = \'review\'
            and log_timestamp > 201801010000
            and log_namespace = 6
            and log_action in (\'approve\', \'approve-i\')
            and page_title = log_title
            and page_namespace = log_namespace
            and page_is_redirect = 0
            order by log_timestamp desc;
        ');
    }


}