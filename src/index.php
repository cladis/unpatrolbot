<?php

use BaseBot\Bot;
use Symfony\Component\Console\Logger;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

use Psr\Log\LogLevel;

require __DIR__ . '/../vendor/autoload.php';

$config = parse_ini_file('config/secure/wiki.ini');
$dbConfig = parse_ini_file('config/secure/db.ini');

$connection = [
    'user' => $dbConfig['user'],
    'password' => $dbConfig['password'],
    'host' => 'ukwiki.analytics.db.svc.eqiad.wmflabs',
    'dbname' => 'ukwiki_p',
    'driver' => 'pdo_mysql',
];

$config['connection'] = $connection;

$verbosityLevelMap = array(
    LogLevel::NOTICE => OutputInterface::VERBOSITY_NORMAL,
    LogLevel::INFO => OutputInterface::VERBOSITY_NORMAL,
);
/**
 * @var \Psr\Log\LoggerInterface
 */
$logger = new Logger\ConsoleLogger(new ConsoleOutput(), $verbosityLevelMap);
$bot = new Bot($config, $logger);
$bot->run();
